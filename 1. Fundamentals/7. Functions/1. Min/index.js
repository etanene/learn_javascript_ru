'use strict'

function min(a, b) {
	return (a > b ? b : a);
}

console.log(min(2, 3));
console.log(min(3, -1));
console.log(min(1, 1));

