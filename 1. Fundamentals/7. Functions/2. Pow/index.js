'use strict'

let num = 0;
let exp = 1;

do {
	num = prompt("Input num", '0');
} while (num < 0 || !num)

do {
	exp = prompt("Input exponent", '1');
} while (exp < 1 || !exp)

alert(pow(num, exp));

function pow(num, exp) {
	let res = num;

	while (--exp) {
		res *= num;
	}
	return (res);
}
