'use strict'

function Calculator() {
	this.read = () => {
		this.a = +prompt('Input first value', '0');
		this.b = +prompt('Input second value', '0');
	};

	this.sum = () => {
		return (this.a + this.b);
	};

	this.mul = () => {
		return (this.a * this.b);
	};
}

let calculator = new Calculator();

calculator.read();
alert(calculator.sum());
alert(calculator.mul());
