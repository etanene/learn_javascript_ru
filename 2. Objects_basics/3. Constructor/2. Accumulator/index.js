'use strict'

function Accumulator(startingValue) {
	this.value = startingValue;

	this.read = () => {
		this.value += +prompt('Input value', 0);
	};
}

let acc = new Accumulator(2);

acc.read();
acc.read();

alert(acc.value);
