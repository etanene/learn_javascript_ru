'use strict'

function readNumber() {
	let input;

	do {
		input = prompt('Input number', 0);
	} while (!isFinite(input));

	if (!input) {
		return (null);
	}

	return (+input);
}

alert(readNumber());
