'use strict'

function ucFirst(str) {
	if (str) {
		return (str[0].toUpperCase() + str.slice(1));
	}
	return (str);
}

let str = '';

console.log(ucFirst(str));
