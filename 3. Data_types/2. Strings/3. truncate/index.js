'use strict'

function truncate(str, maxlen) {
	if (str.length > maxlen) {
		return (str.substr(0, maxlen - 1) + '...');
	}
	return (str);
}

console.log(truncate("What I'd like to tell on this topic is:", 20));
console.log(truncate("Hi everyone!", 20));
