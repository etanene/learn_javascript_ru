'use strict'

function extractCurrency(str) {
	return (+str.slice(1));
}

console.log(extractCurrency('$120') === 120);
