'use strict'

let styles = ['Jazz', 'Blues'];

styles.push('Rock-n-Roll');
styles.splice(styles.length / 2, 1, 'Classics');
console.log(styles.shift());
styles.unshift('Rap', 'Reggae');

console.log(styles);
