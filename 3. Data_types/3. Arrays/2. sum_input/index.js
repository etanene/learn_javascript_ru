'use strict'

let nums = [];

while (true) {
	let input = prompt('Input num', 0);

	if (!input || isNaN(input)) {
		break ;
	}
	nums.push(+input);
}

let sum = 0;
for (let i = 0; i < nums.length; i++) {
	sum += nums[i];
}

alert (sum);
