'use strict'

function maxSum(arr) {
	let max = 0;
	let curr = 0;

	for (let num of arr) {
		curr += num;
		if (curr > 0) {
			if (curr > max) {
				max = curr;
			}
		} else {
			curr = 0;
		}
	}
	return (max);
}

console.log(maxSum([-1, 2, 3, -9]));
console.log(maxSum([2, -1, 2, 3, -9]));
console.log(maxSum([-1, 2, 3, -9, 11]));
console.log(maxSum([-2, -1, 1, 2]));
console.log(maxSum([100, -9, 2, -3, 5]));
console.log(maxSum([1, 2, 3]));
