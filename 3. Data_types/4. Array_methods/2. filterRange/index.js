'use strict'

function filterRange(arr, min, max) {
	return (arr.filter((item) => {
		return (item >= min && item <= max);
	}))
}

let arr = [5, 3, 8, 1];

let res = filterRange(arr, 1, 4);

console.log(arr);
console.log(res);
