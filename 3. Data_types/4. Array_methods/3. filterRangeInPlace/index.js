'use strict'

function filterRangeInPlace(arr, min, max) {
	arr.forEach((item, ind) => {
		if (item < min || item > max) {
			arr.splice(ind, 1);
		}
	});
}

let arr = [5, 3, 8, 1];

console.log(arr);

filterRangeInPlace(arr, 1, 4);

console.log(arr);
