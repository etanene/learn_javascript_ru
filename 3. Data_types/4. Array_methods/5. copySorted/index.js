'use strict'

function copySorted(arr) {
	let res;

	res = arr.slice(0);
	res.sort();
	return (res);
}

let arr = ['HTML', 'JavaScript', 'CSS'];

let res = copySorted(arr);

console.log(arr);
console.log(res);
